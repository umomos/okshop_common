import 'package:flutter_test/flutter_test.dart';

import 'package:okshop_common/okshop_common.dart';

void main() {
  group('num_format', () {
    final input = 1234.567;
    test('currencyStyle', () {
      expect(input.currencyStyle, '\$1,235');
    });

    test('decimalStyle', () {
      expect(input.decimalStyle, '1,235');
    });

    test('floatingStyle', () {
      expect(1234.567.floatingStyle, '1,234.57');
    });
  });
}
