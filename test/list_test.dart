import 'package:flutter_test/flutter_test.dart';
import 'package:okshop_common/okshop_common.dart';

void main() {
  test('distinct1', () {
    final input = [1, 1, 2, 2, 3, 3];
    final actual = input.distinct();
    expect(actual, [1, 2, 3]);
  });

  test('distinct2', () {
    final input = [
      {
        'key': 1,
        'val': 1,
      },
      {
        'key': 1,
        'val': 1,
      },
      {
        'key': 2,
        'val': 2,
      },
      {
        'key': 2,
        'val': 2,
      },
      {
        'key': 3,
        'val': 3,
      },
      {
        'key': 3,
        'val': 3,
      },
    ];
    final wanted = [
      {
        'key': 1,
        'val': 1,
      },
      {
        'key': 2,
        'val': 2,
      },
      {
        'key': 3,
        'val': 3,
      },
    ];
    final actual = input.distinct((e) => e['key']);
    expect(actual, wanted);
  });
}
