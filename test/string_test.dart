import 'package:flutter_test/flutter_test.dart';

import 'package:okshop_common/okshop_common.dart';

void main() {
  group('time', () {
    final timeString = "2021-11-12T08:46:47";
    test('utc at', () {
      expect(timeString.utcAt, DateTime.utc(2021, 11, 12, 8, 46, 47));
    });

    test('local at', () {
      expect(timeString.localAt, DateTime(2021, 11, 12, 16, 46, 47));
    });
  });

  group('take last', () {
    test('take last over lenght', () {
      expect('0123456789'.takeLast(3), '789');
    });

    test('take last less lenght', () => expect('89'.takeLast(3), ' 89'));
  });

  group('full path', () {
    final fullname = 'abc/def/ghij.json';
    test('path', () => expect(fullname.path, 'abc/def'));
    test('filename', () => expect(fullname.filename, 'ghij.json'));
  });
}
