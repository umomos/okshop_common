import 'package:flutter_test/flutter_test.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:okshop_common/okshop_common.dart';

void main() {
  group('DateTime format', () {
    const timeString = '2021-07-14T16:41:59';

    group('utc', () {
      final dateTime = timeString.utcAt;

      test('Full', () {
        expect(dateTime.yMMddHHmmss, '2021/07/14 16:41:59');
      });
      test('Order', () {
        expect(dateTime.MM_ddHHmm, '07-14 16:41');
      });
      test('Revenue', () {
        expect(dateTime.y_MM_dd, '2021-07-14');
      });
    });

    group('local', () {
      final dateTime = timeString.localAt;
      test('Full', () {
        expect(dateTime.yMMddHHmmss, '2021/07/15 00:41:59');
      });
      test('Order', () {
        expect(dateTime.MM_ddHHmm, '07-15 00:41');
      });
      test('Revenue', () {
        expect(dateTime.y_MM_dd, '2021-07-15');
      });
    });
  });

  group('date time format test', () {
    final input = DateTime(2021, 9, 14, 16, 0, 15);

    test('2021/09/14 16:00:15', () {
      expect(input.yMMddHHmmss, '2021/09/14 16:00:15');
    });

    test('2021-09-14 16:00:15', () {
      expect(input.y_MM_ddHHmmss, '2021-09-14 16:00:15');
    });

    test('2021/9/14 16:00', () {
      expect(input.yMdHHmm, '2021/9/14 16:00');
    });

    test('2021-09-14', () {
      expect(input.y_MM_dd, '2021-09-14');
    });

    test('09-14 16:00', () {
      expect(input.MM_ddHHmm, '09-14 16:00');
    });

    test('9月14日 週二', () async {
      await initializeDateFormatting();
      expect(input.MMMEd, '9月14日 週二');
    });

    group('default locale', () {
      final locale = null;
      // final locale = 'en_US';
      test('d', () async {
        await initializeDateFormatting();
        expect(DateFormat.d(locale).format(input), '14');
      });
      test('E', () async {
        await initializeDateFormatting();
        expect(DateFormat.E(locale).format(input), 'Tue');
      });

      test('EEEE', () async {
        await initializeDateFormatting();
        expect(DateFormat.E(locale).format(input), 'Tue');
      });

      test('LLL', () async {
        await initializeDateFormatting();
        expect(DateFormat.LLL(locale).format(input), 'Sep');
      });

      test('LLLL', () async {
        await initializeDateFormatting();
        expect(DateFormat.LLLL(locale).format(input), 'September');
      });

      test('M', () async {
        await initializeDateFormatting();
        expect(DateFormat.M(locale).format(input), '9');
      });

      test('Md', () async {
        await initializeDateFormatting();
        expect(DateFormat.Md(locale).format(input), '9/14');
      });

      test('MEd', () async {
        await initializeDateFormatting();
        expect(DateFormat.MEd(locale).format(input), 'Tue, 9/14');
      });

      test('MMM', () async {
        await initializeDateFormatting();
        expect(DateFormat.MMM(locale).format(input), 'Sep');
      });

      test('MMMd', () async {
        await initializeDateFormatting();
        expect(DateFormat.MMMd(locale).format(input), 'Sep 14');
      });

      test('MMMEd', () async {
        await initializeDateFormatting();
        expect(DateFormat.MMMEd(locale).format(input), 'Tue, Sep 14');
      });

      test('MMMM', () async {
        await initializeDateFormatting();
        expect(DateFormat.MMMM(locale).format(input), 'September');
      });

      test('MMMMd', () async {
        await initializeDateFormatting();
        expect(DateFormat.MMMMd(locale).format(input), 'September 14');
      });

      test('MMMMEEEEd', () async {
        await initializeDateFormatting();
        expect(DateFormat.MMMMEEEEd(locale).format(input),
            'Tuesday, September 14');
      });

      test('QQQ', () async {
        await initializeDateFormatting();
        expect(DateFormat.QQQ(locale).format(input), 'Q3');
      });

      test('QQQQ', () async {
        await initializeDateFormatting();
        expect(DateFormat.QQQQ(locale).format(input), '3rd quarter');
      });

      test('y', () async {
        await initializeDateFormatting();
        expect(DateFormat.y(locale).format(input), '2021');
      });

      test('yM', () async {
        await initializeDateFormatting();
        expect(DateFormat.yM(locale).format(input), '9/2021');
      });

      test('yMd', () async {
        await initializeDateFormatting();
        expect(DateFormat.yMd(locale).format(input), '9/14/2021');
      });

      test('yMEd', () async {
        await initializeDateFormatting();
        expect(DateFormat.yMEd(locale).format(input), 'Tue, 9/14/2021');
      });

      test('yMMM', () async {
        await initializeDateFormatting();
        expect(DateFormat.yMMM(locale).format(input), 'Sep 2021');
      });

      test('yMMMd', () async {
        await initializeDateFormatting();
        expect(DateFormat.yMMMd(locale).format(input), 'Sep 14, 2021');
      });

      test('yMMMEd', () async {
        await initializeDateFormatting();
        expect(DateFormat.yMMMEd(locale).format(input), 'Tue, Sep 14, 2021');
      });

      test('yMMMM', () async {
        await initializeDateFormatting();
        expect(DateFormat.yMMMM(locale).format(input), 'September 2021');
      });

      test('yMMMMd', () async {
        await initializeDateFormatting();
        expect(DateFormat.yMMMMd(locale).format(input), 'September 14, 2021');
      });

      test('yMMMMEEEEd', () async {
        await initializeDateFormatting();
        expect(DateFormat.yMMMMEEEEd(locale).format(input),
            'Tuesday, September 14, 2021');
      });

      test('yQQQ', () async {
        await initializeDateFormatting();
        expect(DateFormat.yQQQ(locale).format(input), 'Q3 2021');
      });

      test('yQQQQ', () async {
        await initializeDateFormatting();
        expect(DateFormat.yQQQQ(locale).format(input), '3rd quarter 2021');
      });

      test('H', () async {
        await initializeDateFormatting();
        expect(DateFormat.H(locale).format(input), '16');
      });

      test('Hm', () async {
        await initializeDateFormatting();
        expect(DateFormat.Hm(locale).format(input), '16:00');
      });

      test('Hms', () async {
        await initializeDateFormatting();
        expect(DateFormat.Hms(locale).format(input), '16:00:15');
      });

      test('j', () async {
        await initializeDateFormatting();
        expect(DateFormat.j(locale).format(input), '4 PM');
      });

      test('jm', () async {
        await initializeDateFormatting();
        expect(DateFormat.jm(locale).format(input), '4:00 PM');
      });

      test('jms', () async {
        await initializeDateFormatting();
        expect(DateFormat.jms(locale).format(input), '4:00:15 PM');
      });
    });

    group('locale', () {
      final locale = 'zh_TW';
      test('d', () async {
        await initializeDateFormatting();
        expect(DateFormat.d(locale).format(input), '14日');
      });
      test('E', () async {
        await initializeDateFormatting();
        expect(DateFormat.E(locale).format(input), '週二');
      });

      test('EEEE', () async {
        await initializeDateFormatting();
        expect(DateFormat.E(locale).format(input), '週二');
      });

      test('LLL', () async {
        await initializeDateFormatting();
        expect(DateFormat.LLL(locale).format(input), '9月');
      });

      test('LLLL', () async {
        await initializeDateFormatting();
        expect(DateFormat.LLLL(locale).format(input), '9月');
      });

      test('M', () async {
        await initializeDateFormatting();
        expect(DateFormat.M(locale).format(input), '9月');
      });

      test('Md', () async {
        await initializeDateFormatting();
        expect(DateFormat.Md(locale).format(input), '9/14');
      });

      test('MEd', () async {
        await initializeDateFormatting();
        expect(DateFormat.MEd(locale).format(input), '9/14（週二）');
      });

      test('MMM', () async {
        await initializeDateFormatting();
        expect(DateFormat.MMM(locale).format(input), '9月');
      });

      test('MMMd', () async {
        await initializeDateFormatting();
        expect(DateFormat.MMMd(locale).format(input), '9月14日');
      });

      test('MMMEd', () async {
        await initializeDateFormatting();
        expect(DateFormat.MMMEd(locale).format(input), '9月14日 週二');
      });

      test('MMMM', () async {
        await initializeDateFormatting();
        expect(DateFormat.MMMM(locale).format(input), '9月');
      });

      test('MMMMd', () async {
        await initializeDateFormatting();
        expect(DateFormat.MMMMd(locale).format(input), '9月14日');
      });

      test('MMMMEEEEd', () async {
        await initializeDateFormatting();
        expect(DateFormat.MMMMEEEEd(locale).format(input), '9月14日 星期二');
      });

      test('QQQ', () async {
        await initializeDateFormatting();
        expect(DateFormat.QQQ(locale).format(input), '第3季');
      });

      test('QQQQ', () async {
        await initializeDateFormatting();
        expect(DateFormat.QQQQ(locale).format(input), '第3季');
      });

      test('y', () async {
        await initializeDateFormatting();
        expect(DateFormat.y(locale).format(input), '2021年');
      });

      test('yM', () async {
        await initializeDateFormatting();
        expect(DateFormat.yM(locale).format(input), '2021/9');
      });

      test('yMd', () async {
        await initializeDateFormatting();
        expect(DateFormat.yMd(locale).format(input), '2021/9/14');
      });

      test('yMEd', () async {
        await initializeDateFormatting();
        expect(DateFormat.yMEd(locale).format(input), '2021/9/14（週二）');
      });

      test('yMMM', () async {
        await initializeDateFormatting();
        expect(DateFormat.yMMM(locale).format(input), '2021年9月');
      });

      test('yMMMd', () async {
        await initializeDateFormatting();
        expect(DateFormat.yMMMd(locale).format(input), '2021年9月14日');
      });

      test('yMMMEd', () async {
        await initializeDateFormatting();
        expect(DateFormat.yMMMEd(locale).format(input), '2021年9月14日 週二');
      });

      test('yMMMM', () async {
        await initializeDateFormatting();
        expect(DateFormat.yMMMM(locale).format(input), '2021年9月');
      });

      test('yMMMMd', () async {
        await initializeDateFormatting();
        expect(DateFormat.yMMMMd(locale).format(input), '2021年9月14日');
      });

      test('yMMMMEEEEd', () async {
        await initializeDateFormatting();
        expect(DateFormat.yMMMMEEEEd(locale).format(input), '2021年9月14日 星期二');
      });

      test('yQQQ', () async {
        await initializeDateFormatting();
        expect(DateFormat.yQQQ(locale).format(input), '2021年第3季');
      });

      test('yQQQQ', () async {
        await initializeDateFormatting();
        expect(DateFormat.yQQQQ(locale).format(input), '2021年第3季');
      });

      test('H', () async {
        await initializeDateFormatting();
        expect(DateFormat.H(locale).format(input), '16時');
      });

      test('Hm', () async {
        await initializeDateFormatting();
        expect(DateFormat.Hm(locale).format(input), '16:00');
      });

      test('Hms', () async {
        await initializeDateFormatting();
        expect(DateFormat.Hms(locale).format(input), '16:00:15');
      });

      test('j', () async {
        await initializeDateFormatting();
        expect(DateFormat.j(locale).format(input), '下午4時');
      });

      test('jm', () async {
        await initializeDateFormatting();
        expect(DateFormat.jm(locale).format(input), '下午4:00');
      });

      test('jms', () async {
        await initializeDateFormatting();
        expect(DateFormat.jms(locale).format(input), '下午4:00:15');
      });
    });
  });
}
