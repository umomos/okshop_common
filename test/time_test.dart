import 'package:flutter_test/flutter_test.dart';
import 'package:okshop_common/okshop_common.dart';

main() {
  group('time', () {
    const timeString = '2021-07-14T16:41:59';
    test('utc', () {
      final date = DateTime.tryParse(timeString);
      // final a = date.toIso8601String(); // "2021-07-14T16:41:59.000"
      // final b = date.toLocal(); // DateTime (2021-07-14 16:41:59.000)
      // final c = date.toString(); // "2021-07-14 16:41:59.000"
      // final d = date.toUtc(); // DateTime (2021-07-14 08:41:59.000Z)
      expect(date.isUtc, false);
    });

    test('local', () {
      final date = DateTime.tryParse('${timeString}z');
      expect(date.isUtc, true);
    });

    test('utcToLocal', () {
      final dateTime = timeString.localAt;
      expect(dateTime.day, 15);
      expect(dateTime.hour, 0);
    });

    test('empty string', () {
      final localAt = ''.localAt;
      expect(localAt, null);
    });

    test('null', () {
      final String nil = null;
      final localAt = nil.localAt;
      expect(localAt, null);
    });

    test('string extension', () {
      final dateTime = timeString.localAt;
      expect(dateTime.day, 15);
      expect(dateTime.hour, 0);
    });
  });
}
