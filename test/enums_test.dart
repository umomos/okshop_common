import 'package:flutter_test/flutter_test.dart';

import 'package:okshop_common/okshop_common.dart';

main() {
  group('AddressType', () {
    test('購買人', () {
      expect(0.addressType, AddressType.Buyer);
      expect(0.addressType.isRecipient, false);
      expect(0.addressType.isBuyer, true);
      expect(0.addressType, AddressType.Buyer);
      expect(AddressType.Buyer.name, '購買人');
    });

    test('收件人', () {
      expect(1.addressType, AddressType.Recipient);
      expect(1.addressType.isRecipient, true);
      expect(1.addressType.isBuyer, false);
      expect(AddressType.Recipient.name, '收件人');
    });
  });

  group('物流類型', () {
    test('自取', () {
      expect(1.shippingMethod, ShippingMethod.PickUp);
      expect(ShippingMethod.PickUp.name, '自取');
    });

    test('宅配：常溫', () {
      expect(2.shippingMethod, ShippingMethod.Amb);
      expect(ShippingMethod.Amb.name, '常溫');
    });

    test('宅配：低溫', () {
      expect(3.shippingMethod, ShippingMethod.Cold);
      expect(ShippingMethod.Cold.name, '低溫');
    });
  });

  group('載具類別', () {
    test('悠遊卡', () {
      expect(0.carrierType, CarrierType.EasyCard);
      expect(CarrierType.EasyCard.name, '悠遊卡');
    });

    test('一卡通', () {
      expect(1.carrierType, CarrierType.iPass);
      expect(CarrierType.iPass.name, '一卡通');
    });

    test('icash', () {
      expect(2.carrierType, CarrierType.icash);
      expect(CarrierType.icash.name, 'icash');
    });

    test('手機', () {
      expect(3.carrierType, CarrierType.Mobile);
      expect(CarrierType.Mobile.name, '手機');
    });

    test('自然人憑證', () {
      expect(4.carrierType, CarrierType.Certification);
      expect(CarrierType.Certification.name, '自然人憑證');
    });

    test('金融卡', () {
      expect(5.carrierType, CarrierType.Debit);
      expect(CarrierType.Debit.name, '金融卡');
    });

    test('公用事業', () {
      expect(6.carrierType, CarrierType.Public);
      expect(CarrierType.Public.name, '公用事業');
    });

    test('信用卡', () {
      expect(7.carrierType, CarrierType.CreditCard);
      expect(CarrierType.CreditCard.name, '信用卡');
    });

    test('會員', () {
      expect(8.carrierType, CarrierType.Member);
      expect(CarrierType.Member.name, '會員');
    });
  });

  group('訂單狀態', () {
    test('處理中', () {
      expect(0.orderStatus, OrderStatus.Padding);
      expect(OrderStatus.Padding.name, '處理中');
      expect(OrderStatus.Padding.color, StatusColor.Normal);
    });

    test('已確認', () {
      expect(1.orderStatus, OrderStatus.Accepted);
      expect(OrderStatus.Accepted.name, '已確認');
      expect(OrderStatus.Accepted.color, StatusColor.Normal);
    });

    test('訂單完成', () {
      expect(2.orderStatus, OrderStatus.Completed);
      expect(OrderStatus.Completed.name, '訂單完成');
      expect(OrderStatus.Completed.color, StatusColor.Normal);
    });

    test('訂單取消 (店家)', () {
      expect(3.orderStatus, OrderStatus.CancelByApp);
      expect(OrderStatus.CancelByApp.name, '訂單取消');
      expect(OrderStatus.CancelByApp.color, StatusColor.Error);
    });

    test('訂單異常', () {
      expect(4.orderStatus, OrderStatus.Exception);
      expect(OrderStatus.Exception.name, '訂單異常');
      expect(OrderStatus.Exception.color, StatusColor.Error);
    });

    test('訂單退貨、退款', () {
      expect(5.orderStatus, OrderStatus.Rejection);
      expect(OrderStatus.Rejection.name, '訂單退貨、退款');
      expect(OrderStatus.Rejection.color, StatusColor.Error);
    });

    test('訂單取消 (消費者)', () {
      expect(6.orderStatus, OrderStatus.CancelByLine);
      expect(OrderStatus.CancelByLine.name, '訂單取消');
      expect(OrderStatus.CancelByLine.color, StatusColor.Error);
    });

    test('OutOfRange', () {
      expect((-1).orderStatus, OrderStatus.Max);
      expect(OrderStatus.Max.name, '');
      expect(OrderStatus.Max.color, OKColor.Error);
    });
  });

  group('角色', () {
    test('店長', () {
      expect(1.storeRole, StoreRole.Boss);
      expect(1.storeRole.name, '店長');
      expect(1.storeRole.isBoss, true);
      expect(1.storeRole.isAny, true);
      expect(1.storeRole.isEmployee, false);
    });
    test('店員', () {
      expect(2.storeRole, StoreRole.Employee);
      expect(2.storeRole.name, '店員');
      expect(2.storeRole.isBoss, false);
      expect(2.storeRole.isAny, true);
      expect(2.storeRole.isEmployee, true);
    });
  });

  group('產品分類', () {
    test('餐飲店內', () {
      expect(0.productKind, ProductKind.DinnerApp);
      expect(0.productKind.name, '餐飲店內');
      expect(0.productKind.isDinnerApp, true);
    });
    test('餐飲線上', () {
      expect(1.productKind, ProductKind.DinnerLine);
      expect(1.productKind.name, '餐飲線上');
      expect(1.productKind.isDinnerLine, true);
    });
    test('零售', () {
      expect(2.productKind, ProductKind.Retail);
      expect(2.productKind.name, '零售');
      expect(2.productKind.isRetail, true);
    });
  });

  group('選項', () {
    test('單選', () {
      expect(0.selection, Selection.Single);
      expect(0.selection.isSingle, true);
      expect(0.selection.isMultiple, false);
    });
    test('複選', () {
      expect(1.selection, Selection.Multiple);
      expect(1.selection.isSingle, false);
      expect(1.selection.isMultiple, true);
    });
  });

  group('Requirement', () {
    test('非必填', () {
      expect(0.requirement, Requirement.NiceToHave);
      expect(0.requirement.isNiceToHave, true);
      expect(0.requirement.isMustToHave, false);
    });
    test('必填', () {
      expect(1.requirement, Requirement.MustToHave);
      expect(1.requirement.isNiceToHave, false);
      expect(1.requirement.isMustToHave, true);
    });
  });

  group('物流類別', () {
    test('常溫', () {
      expect(0.shippingType, ShippingType.Normal);
      expect(0.shippingType.name, '常溫');
      expect(0.shippingType.isNormal, true);
      expect(0.shippingType.isCold, false);
    });
    test('低溫', () {
      expect(1.shippingType, ShippingType.Cold);
      expect(1.shippingType.name, '低溫');
      expect(1.shippingType.isNormal, false);
      expect(1.shippingType.isCold, true);
    });
  });

  group('品牌業態', () {
    test('餐飲-前結帳', () {
      expect(0.brandsType, BrandsType.BeforeDinner);
      expect(BrandsType.BeforeDinner.name, '餐飲-前結帳');
    });
    test('餐飲-後結帳', () {
      expect(1.brandsType, BrandsType.AfterDinner);
      expect(BrandsType.AfterDinner.name, '餐飲-後結帳');
    });
    test('零售', () {
      expect(2.brandsType, BrandsType.Retail);
      expect(BrandsType.Retail.name, '零售');
    });
    test('餐飲-前結帳+零售', () {
      expect(3.brandsType, BrandsType.BeforeDinnerWithRetail);
      expect(BrandsType.BeforeDinnerWithRetail.name, '餐飲-前結帳+零售');
    });
    test('餐飲-後結帳+零售', () {
      expect(4.brandsType, BrandsType.AfterDinnerWithRetail);
      expect(BrandsType.AfterDinnerWithRetail.name, '餐飲-後結帳+零售');
    });
    test('OutOfRange', () {
      expect(8.brandsType, BrandsType.Max);
      expect(BrandsType.Max.name, '');
    });
  });

  group('開關', () {
    test('關閉', () {
      expect(0.switcher, Switcher.Off);
      expect(0.switcher.isOff, true);
      expect(0.switcher.isOn, false);
    });
    test('開啟', () {
      expect(1.switcher, Switcher.On);
      expect(1.switcher.isOn, true);
      expect(1.switcher.isOff, false);
    });
  });

  group('稅金', () {
    test('應稅', () {
      expect(1.taxType, TaxType.TX);
      expect(TaxType.TX.name, '應稅');
    });
    test('免稅', () {
      expect(2.taxType, TaxType.Free);
      expect(TaxType.Free.name, '免稅');
    });
    test('混合稅率', () {
      expect(3.taxType, TaxType.Mix);
      expect(TaxType.Mix.name, '混合稅率');
    });
    test('OutOfRange', () {
      expect(4.taxType, TaxType.Max);
      expect(TaxType.Max.name, '');
    });
  });

  group('Button', () {
    test('Negative', () {
      expect(0.button, Button.Negative);
      expect(0.button.isNegative, true);
    });
    test('Positive', () {
      expect(1.button, Button.Positive);
      expect(1.button.isPositive, true);
    });
    test('Middle', () {
      expect(2.button, Button.Middle);
      expect(2.button.isMiddle, true);
    });
  });

  group('商店類型', () {
    test('餐飲內用', () {
      expect(OrderType.DinnerHere.storeType, StoreType.Dinner);
      expect(OrderType.DinnerHere.storeType.isDinner, true);
      expect(OrderType.DinnerHere.storeType.isRetail, false);
    });
    test('餐飲外帶/餐飲自取', () {
      expect(OrderType.DinnerToGo.storeType, StoreType.Dinner);
      expect(OrderType.DinnerToGo.storeType.isDinner, true);
      expect(OrderType.DinnerToGo.storeType.isRetail, false);
    });
    test('餐飲外送', () {
      expect(OrderType.DinnerDelivery.storeType, StoreType.Dinner);
      expect(OrderType.DinnerDelivery.storeType.isDinner, true);
      expect(OrderType.DinnerDelivery.storeType.isRetail, false);
    });
    test('零售自取', () {
      expect(OrderType.RetailToGo.storeType, StoreType.Retail);
      expect(OrderType.RetailToGo.storeType.isDinner, false);
      expect(OrderType.RetailToGo.storeType.isRetail, true);
    });
    test('零售宅配', () {
      expect(OrderType.RetailDelivery.storeType, StoreType.Retail);
      expect(OrderType.RetailDelivery.storeType.isDinner, false);
      expect(OrderType.RetailDelivery.storeType.isRetail, true);
    });
    test('零售超商', () {
      expect(OrderType.RetailInStore.storeType, StoreType.Retail);
      expect(OrderType.RetailInStore.storeType.isDinner, false);
      expect(OrderType.RetailInStore.storeType.isRetail, true);
    });
  });

  group('訂單類型', () {
    test('餐飲內用', () {
      expect(0.orderType, OrderType.DinnerHere);
      expect(OrderType.DinnerHere.name, '內用');
      expect(OrderType.DinnerHere.color, OrderTypeColor.DinnerHere);
    });
    test('餐飲外帶/餐飲自取', () {
      expect(1.orderType, OrderType.DinnerToGo);
      expect(OrderType.DinnerToGo.name, '自取');
      expect(OrderType.DinnerToGo.color, OrderTypeColor.DinnerToGo);
    });
    test('餐飲外送', () {
      expect(2.orderType, OrderType.DinnerDelivery);
      expect(OrderType.DinnerDelivery.name, '外送');
      expect(OrderType.DinnerDelivery.color, OrderTypeColor.DinnerDelivery);
    });
    test('零售自取', () {
      expect(3.orderType, OrderType.RetailToGo);
      expect(OrderType.RetailToGo.name, '自取');
      expect(OrderType.RetailToGo.color, OrderTypeColor.RetailToGo);
    });
    test('零售宅配', () {
      expect(4.orderType, OrderType.RetailDelivery);
      expect(OrderType.RetailDelivery.name, '宅配');
      expect(OrderType.RetailDelivery.color, OrderTypeColor.RetailDelivery);
    });
    test('零售超商', () {
      expect(5.orderType, OrderType.RetailInStore);
      expect(OrderType.RetailInStore.name, '超取');
      expect(OrderType.RetailInStore.color, OrderTypeColor.RetailInStore);
    });
    test('OutOfRange', () {
      expect(6.orderType, OrderType.Max);
      expect(OrderType.Max.name, '');
      expect(OrderType.Max.color, OKColor.Error);
    });
  });

  group('訂單來源', () {
    test('APP 訂單', () {
      expect(0.orderSource, OrderSource.App);
      expect(OrderSource.App.name, 'APP 訂單');
    });
    test('LINE 訂單', () {
      expect(1.orderSource, OrderSource.Line);
      expect(OrderSource.Line.name, 'LINE 訂單');
    });
    test('OutOfRange', () {
      expect(2.orderSource, OrderSource.Max);
      expect(OrderSource.Max.name, '');
    });
  });

  group('付款狀態', () {
    test('未付款', () {
      expect(0.paymentStatus, PaymentStatus.Outstanding);
      expect(PaymentStatus.Outstanding.name, '未付款');
      expect(PaymentStatus.Outstanding.color, OKColor.Error);
    });
    test('未結清', () {
      expect(1.paymentStatus, PaymentStatus.Balance);
      expect(PaymentStatus.Balance.name, '未結清');
      expect(PaymentStatus.Balance.color, OKColor.Error);
    });
    test('已付款', () {
      expect(2.paymentStatus, PaymentStatus.Paid);
      expect(PaymentStatus.Paid.name, '已付款');
      expect(PaymentStatus.Paid.color, OKColor.Retail);
    });
    test('付款失敗', () {
      expect(3.paymentStatus, PaymentStatus.Failed);
      expect(PaymentStatus.Failed.name, '付款失敗');
      expect(PaymentStatus.Failed.color, OKColor.Error);
    });
    test('超過付款時間', () {
      expect(4.paymentStatus, PaymentStatus.Timeout);
      expect(PaymentStatus.Timeout.name, '超過付款時間');
      expect(PaymentStatus.Timeout.color, OKColor.Error);
    });
    test('OutOfRange', () {
      expect(5.paymentStatus, PaymentStatus.Max);
      expect(PaymentStatus.Max.name, '');
      expect(PaymentStatus.Max.color, OKColor.Error);
    });
  });

  // FIXME:
  group('線下金流類型 (APP)', () {
    test('現金', () {
      expect(1.appPayMethod, AppPayMethod.Cash);
      expect(AppPayMethod.Cash.name, '現金');
    });
    test('信用卡', () {
      expect(2.appPayMethod, AppPayMethod.CreditCard);
      expect(AppPayMethod.CreditCard.name, '信用卡');
    });
    // test('台灣Pay', () {
    //   expect(3.appPaymentMethod, AppPaymentMethod.Max);
    //   expect(AppPaymentMethod.Max.name, '台灣Pay');
    // });
    // test('支付寶', () {
    //   expect(4.appPaymentMethod, AppPaymentMethod.Max);
    //   expect(AppPaymentMethod.Max.name, '支付寶');
    // });
    // test('Line Pay Money', () {
    //   expect(5.appPaymentMethod, AppPaymentMethod.Max);
    //   expect(AppPaymentMethod.Max.name, 'Line Pay Money');
    // });
    // test('iCash', () {
    //   expect(6.appPaymentMethod, AppPaymentMethod.Max);
    //   expect(AppPaymentMethod.Max.name, 'icash');
    // });
    // test('微信支付', () {
    //   expect(7.appPaymentMethod, AppPaymentMethod.Max);
    //   expect(AppPaymentMethod.Max.name, '微信支付');
    // });
    // test('Happy Cash', () {
    //   expect(8.appPaymentMethod, AppPaymentMethod.Max);
    //   expect(AppPaymentMethod.Max.name, 'Happy Cash');
    // });
    // test('轉帳匯款', () {
    //   expect(9.appPaymentMethod, AppPaymentMethod.Max);
    //   expect(AppPaymentMethod.Max.name, '轉帳匯款');
    // });
    // test('Line Pay', () {
    //   expect(10.appPaymentMethod, AppPaymentMethod.Max);
    //   expect(AppPaymentMethod.Max.name, 'Line Pay');
    // });
    // test('悠遊卡 Easy Card', () {
    //   expect(11.appPaymentMethod, AppPaymentMethod.Max);
    //   expect(AppPaymentMethod.Max.name, '悠遊卡 Easy Card');
    // });
    // test('街口支付', () {
    //   expect(12.appPaymentMethod, AppPaymentMethod.Max);
    //   expect(AppPaymentMethod.Max.name, '街口支付');
    // });
    // test('iPass 一卡通', () {
    //   expect(13.appPaymentMethod, AppPaymentMethod.Max);
    //   expect(AppPaymentMethod.Max.name, 'iPass 一卡通');
    // });
    // test('Foodpanda', () {
    //   expect(14.appPaymentMethod, AppPaymentMethod.Max);
    //   expect(AppPaymentMethod.Max.name, 'Foodpanda');
    // });
    // test('歐付寶', () {
    //   expect(15.appPaymentMethod, AppPaymentMethod.Max);
    //   expect(AppPaymentMethod.Max.name, '歐付寶');
    // });
    // test('UberEats 線上金流類型 (WEB)', () {
    //   expect(16.appPaymentMethod, AppPaymentMethod.Max);
    //   expect(AppPaymentMethod.Max.name, 'UberEats 線上金流類型 (WEB)');
    // });
    // test('到店付款', () {
    //   expect(17.appPaymentMethod, AppPaymentMethod.Max);
    //   expect(AppPaymentMethod.Max.name, '到店付款');
    // });
    // test('綠界信用卡', () {
    //   expect(18.appPaymentMethod, AppPaymentMethod.Max);
    //   expect(AppPaymentMethod.Max.name, '綠界信用卡');
    // });
    // test('轉帳匯款', () {
    //   expect(19.appPaymentMethod, AppPaymentMethod.Max);
    //   expect(AppPaymentMethod.Max.name, '轉帳匯款');
    // });
    test('OutOfRange', () {
      expect(22.appPayMethod, AppPayMethod.Max);
      expect(AppPayMethod.Max.name, '');
    });
  });

  group('物流類型', () {
    test('自取', () {
      expect(1.shippingMethod, ShippingMethod.PickUp);
      expect(ShippingMethod.PickUp.name, '自取');
    });
    test('宅配：常溫', () {
      expect(2.shippingMethod, ShippingMethod.Amb);
      expect(ShippingMethod.Amb.name, '常溫');
    });
    test('宅配：低溫', () {
      expect(3.shippingMethod, ShippingMethod.Cold);
      expect(ShippingMethod.Cold.name, '低溫');
    });
    test('OutOfRange', () {
      expect(4.shippingMethod, ShippingMethod.Max);
      expect(ShippingMethod.Max.name, '');
    });
  });

  group('載具類別', () {
    test('悠遊卡', () {
      expect(0.carrierType, CarrierType.EasyCard);
      expect(CarrierType.EasyCard.name, '悠遊卡');
    });
    test('一卡通', () {
      expect(1.carrierType, CarrierType.iPass);
      expect(CarrierType.iPass.name, '一卡通');
    });
    test('icash', () {
      expect(2.carrierType, CarrierType.icash);
      expect(CarrierType.icash.name, 'icash');
    });
    test('手機', () {
      expect(3.carrierType, CarrierType.Mobile);
      expect(CarrierType.Mobile.name, '手機');
    });
    test('自然人憑證', () {
      expect(4.carrierType, CarrierType.Certification);
      expect(CarrierType.Certification.name, '自然人憑證');
    });
    test('金融卡', () {
      expect(5.carrierType, CarrierType.Debit);
      expect(CarrierType.Debit.name, '金融卡');
    });
    test('公用事業', () {
      expect(6.carrierType, CarrierType.Public);
      expect(CarrierType.Public.name, '公用事業');
    });
    test('信用卡', () {
      expect(7.carrierType, CarrierType.CreditCard);
      expect(CarrierType.CreditCard.name, '信用卡');
    });
    test('會員', () {
      expect(8.carrierType, CarrierType.Member);
      expect(CarrierType.Member.name, '會員');
    });
    test('OutOfRange', () {
      expect(9.carrierType, CarrierType.Max);
      expect(CarrierType.Max.name, '');
    });
  });

  group('發票類型', () {
    test('免開發票', () {
      expect(0.invoiceType, InvoiceType.GUIexception);
      expect(InvoiceType.GUIexception.name, '免開發票');
    });
    test('個人電子發票', () {
      expect(1.invoiceType, InvoiceType.Invoice);
      expect(InvoiceType.Invoice.name, '個人電子發票');
    });
    test('公司電子發票/統編', () {
      expect(2.invoiceType, InvoiceType.TaxId);
      expect(InvoiceType.TaxId.name, '公司電子發票');
    });
    test('手機條碼載具', () {
      expect(3.invoiceType, InvoiceType.Carrier);
      expect(InvoiceType.Carrier.name, '手機條碼載具');
    });
    test('愛心碼', () {
      expect(4.invoiceType, InvoiceType.LoveCode);
      expect(InvoiceType.LoveCode.name, '愛心碼');
    });
    test('紙本發票', () {
      expect(5.invoiceType, InvoiceType.Paper);
      expect(InvoiceType.Paper.name, '紙本發票');
    });
    test('OutOfRange', () {
      expect(6.invoiceType, InvoiceType.Max);
      expect(InvoiceType.Max.name, '');
    });
  });

  group('發票狀態', () {
    test('開立', () {
      expect(0.invoiceStatus, InvoiceStatus.Invoice);
      expect(InvoiceStatus.Invoice.name, '開立');
    });
    test('作廢', () {
      expect(1.invoiceStatus, InvoiceStatus.Cancel);
      expect(InvoiceStatus.Cancel.name, '作廢');
    });
    test('折讓', () {
      expect(2.invoiceStatus, InvoiceStatus.Discount);
      expect(InvoiceStatus.Discount.name, '折讓');
    });
    test('OutOfRange', () {
      expect(3.invoiceStatus, InvoiceStatus.Max);
      expect(InvoiceStatus.Max.name, '');
    });
  });

  group('產品類型', () {
    test('一般', () {
      expect(0.itemType, ItemType.Normal);
      expect(ItemType.Normal.name, '一般');
    });

    test('預購', () {
      expect(1.itemType, ItemType.PreOrder);
      expect(ItemType.PreOrder.name, '預購');
    });

    test('加購', () {
      expect(2.itemType, ItemType.OneMore);
      expect(ItemType.OneMore.name, '加購');
    });

    test('贈品', () {
      expect(3.itemType, ItemType.Giveaway);
      expect(ItemType.Giveaway.name, '贈品');
    });

    test('OutOfRange', () {
      expect(4.itemType, ItemType.Max);
      expect(ItemType.Max.name, '');
    });
  });

  group('折扣類別', () {
    test('coupon', () {
      expect(0.discountType, DiscountType.Coupon);
      expect(DiscountType.Coupon.name, 'coupon');
    });

    test('promotion', () {
      expect(1.discountType, DiscountType.Promotion);
      expect(DiscountType.Promotion.name, 'promotion');
    });

    test('整筆改價', () {
      expect(2.discountType, DiscountType.Reset);
      expect(DiscountType.Reset.name, '整筆改價');
    });

    test('現場折扣', () {
      expect(3.discountType, DiscountType.Discount);
      expect(DiscountType.Discount.name, '現場折價');
    });

    test('OutOfRange', () {
      expect(4.discountType, DiscountType.Max);
      expect(DiscountType.Max.name, '');
    });
  });

  group('服務費', () {
    test('關閉', () {
      expect(0.serviceFeeType, ServiceFeeType.None);
    });

    test('以原價計算', () {
      expect(1.serviceFeeType, ServiceFeeType.Origin);
    });

    test('以折扣後價格算', () {
      expect(2.serviceFeeType, ServiceFeeType.Discount);
    });
  });

  group('次數', () {
    test('首次', () {
      expect(0.times, Times.First);
    });
    test('補印', () {
      expect(1.times, Times.Again);
    });
  });

  group('金財通稅別', () {
    test('應稅', () {
      expect(1.bpscmTaxType, BpscmTaxType.TX);
    });

    test('零稅率', () {
      expect(2.bpscmTaxType, BpscmTaxType.Zero);
    });

    test('免稅 (農產品)', () {
      expect(3.bpscmTaxType, BpscmTaxType.Free);
    });

    test('應稅 (特種稅率)', () {
      expect(4.bpscmTaxType, BpscmTaxType.Special);
    });

    test('混合稅率', () {
      expect(9.bpscmTaxType, BpscmTaxType.Mix);
    });
  });

  group('稅別', () {
    test('無稅率', () {
      expect(TaxRate.None.value, 0);
    });

    test('應稅', () {
      expect(TaxRate.TX.value, 0.05);
    });
  });

  group('bpscm enum test', () {
    group('tax type name', () {
      test('normal', () {
        expect(BpscmTaxType.TX.name, '應稅');
      });
      test('zero', () {
        expect(BpscmTaxType.Zero.name, '零稅率');
      });
      test('free', () {
        expect(BpscmTaxType.Free.name, '免稅');
      });
      test('special', () {
        expect(BpscmTaxType.Special.name, '應稅');
      });
      test('mix', () {
        expect(BpscmTaxType.Mix.name, '混合稅率');
      });
    });

    group('tax type str', () {
      test('normal', () {
        expect(BpscmTaxType.TX.str, '1');
      });
      test('zero', () {
        expect(BpscmTaxType.Zero.str, '2');
      });
      test('free', () {
        expect(BpscmTaxType.Free.str, '3');
      });
      test('special', () {
        expect(BpscmTaxType.Special.str, '4');
      });
      test('mix', () {
        expect(BpscmTaxType.Mix.str, '9');
      });
    });

    group('tax type value', () {
      test('normal', () {
        expect(BpscmTaxType.TX.value, 1);
      });
      test('zero', () {
        expect(BpscmTaxType.Zero.value, 2);
      });
      test('free', () {
        expect(BpscmTaxType.Free.value, 3);
      });
      test('special', () {
        expect(BpscmTaxType.Special.value, 4);
      });
      test('mix', () {
        expect(BpscmTaxType.Mix.value, 9);
      });
    });

    group('tax type', () {
      test('none', () {
        expect(0.bpscmTaxType, BpscmTaxType.None);
      });
      test('normal', () {
        expect(1.bpscmTaxType, BpscmTaxType.TX);
      });
      test('zero', () {
        expect(2.bpscmTaxType, BpscmTaxType.Zero);
      });
      test('free', () {
        expect(3.bpscmTaxType, BpscmTaxType.Free);
      });
      test('special', () {
        expect(4.bpscmTaxType, BpscmTaxType.Special);
      });
      test('mix', () {
        expect(9.bpscmTaxType, BpscmTaxType.Mix);
      });
    });

    group('invoice status name', () {
      test('invoice', () {
        expect(BpscmInvoiceStatus.Invoice.name, '發票開立');
      });
      test('cancel', () {
        expect(BpscmInvoiceStatus.Cancel.name, '發票作廢');
      });
      test('allowance', () {
        expect(BpscmInvoiceStatus.Allowance.name, '折讓開立');
      });
      test('allowance cancel', () {
        expect(BpscmInvoiceStatus.AllowanceCancel.name, '折讓作廢');
      });
    });

    group('invoice status value', () {
      test('invoice', () {
        expect(BpscmInvoiceStatus.Invoice.value, 1);
      });
      test('cancel', () {
        expect(BpscmInvoiceStatus.Cancel.value, 2);
      });
      test('allowance', () {
        expect(BpscmInvoiceStatus.Allowance.value, 4);
      });
      test('allowance cancel', () {
        expect(BpscmInvoiceStatus.AllowanceCancel.value, 5);
      });
    });

    group('sort type', () {
      test('ASC', () {
        expect(1.sortType, SortType.ASC);
      });

      test('DESC', () {
        expect(0.sortType, SortType.DESC);
      });

      group('name', () {
        test('ASC', () {
          expect(SortType.ASC.name, 'ASC');
        });

        test('DESC', () {
          expect(SortType.DESC.name, 'DESC');
        });
      });
    });
  });

  group('printer paper size', () {
    group('from num', () {
      test('0', () => expect(0.printerPaperSize, PrinterPaperSize.None));
      test('1', () => expect(1.printerPaperSize, PrinterPaperSize.mm58));
      test('2', () => expect(2.printerPaperSize, PrinterPaperSize.mm80));
      test('3', () => expect(3.printerPaperSize, PrinterPaperSize.Max));
    });
    group('to num', () {
      test('0', () => expect(PrinterPaperSize.None.index, 0));
      test('1', () => expect(PrinterPaperSize.mm58.index, 1));
      test('2', () => expect(PrinterPaperSize.mm80.index, 2));
      test('3', () => expect(PrinterPaperSize.Max.index, 3));
    });
    group('name', () {
      test('58mm', () {
        expect(PrinterPaperSize.mm58.name, '58mm');
      });
      test('80mm', () {
        expect(PrinterPaperSize.mm80.name, '80mm');
      });
    });
  });
}
