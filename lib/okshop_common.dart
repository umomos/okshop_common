library okshop_common;

export './src/keys.dart';
export './src/enums.dart';
export './src/constants.dart';
export './src/extension.dart';
export './src/ok_colors.dart';
export './src/num_extension.dart';
export './src/enums_extension.dart';

/// A Calculator.
// class Calculator {
//   /// Returns [value] plus 1.
//   int addOne(int value) => value + 1;
// }
