import 'dart:ui' show Color;

import 'package:flutter/foundation.dart' show describeEnum;
import 'package:flutter/material.dart' show Colors;

import 'constants.dart';
import 'enums.dart';
import 'ok_colors.dart';

extension ExtensionRole on StoreRole {
  String get name {
    switch (this) {
      case StoreRole.Boss:
        return '店長';
      case StoreRole.Employee:
        return '店員';
      default:
        return '';
    }
  }

  bool get isBoss => StoreRole.Boss == this;

  bool get isEmployee => StoreRole.Employee == this;

  bool get isAny {
    switch (this) {
      case StoreRole.Boss:
      case StoreRole.Employee:
        return true;
      default:
        return false;
    }
  }
}

extension ExtensionProductKind on ProductKind {
  String get name {
    switch (this) {
      case ProductKind.DinnerApp:
        return '餐飲店內';
      case ProductKind.DinnerLine:
        return '餐飲線上';
      case ProductKind.Retail:
        return '零售';
      default:
        return '';
    }
  }

  bool get isDinnerApp => ProductKind.DinnerApp == this;

  bool get isDinnerLine => ProductKind.DinnerLine == this;

  bool get isRetail => ProductKind.Retail == this;
}

extension ExtensionSelection on Selection {
  String get name {
    switch (this) {
      case Selection.Single:
        return '單選';
      case Selection.Multiple:
        return '複選';
      default:
        return '';
    }
  }

  bool get isSingle => Selection.Single == this;

  bool get isMultiple => Selection.Multiple == this;
}

extension ExtensionRequirement on Requirement {
  String get name {
    switch (this) {
      case Requirement.NiceToHave:
        return '選填';
      case Requirement.MustToHave:
        return '必填';
      default:
        return '';
    }
  }

  bool get isNiceToHave => Requirement.NiceToHave == this;

  bool get isMustToHave => Requirement.MustToHave == this;
}

extension ExtensionShippingType on ShippingType {
  String get name {
    switch (this) {
      case ShippingType.Normal:
        return '常溫';
      case ShippingType.Cold:
        return '低溫';
      default:
        return '';
    }
  }

  bool get isNormal => ShippingType.Normal == this;

  bool get isCold => ShippingType.Cold == this;
}

extension ExtensionBrandsType on BrandsType {
  String get name {
    switch (this) {
      case BrandsType.BeforeDinner:
        return '餐飲-前結帳';
      case BrandsType.AfterDinner:
        return '餐飲-後結帳';
      case BrandsType.Retail:
        return '零售';
      case BrandsType.BeforeDinnerWithRetail:
        return '餐飲-前結帳+零售';
      case BrandsType.AfterDinnerWithRetail:
        return '餐飲-後結帳+零售';
      default:
        return '';
    }
  }

  bool contains(StoreType value) {
    switch (value) {
      case StoreType.Dinner:
        return containsDinner;
      case StoreType.Retail:
        return containsRetail;
      default:
        return false;
    }
  }

  bool get containsDinner => BRANDS_TYPE_DINNER.contains(this);

  bool get containsRetail => BRANDS_TYPE_RETAIL.contains(this);

  num get storeTypeCount {
    var count = 0;
    if (containsDinner) {
      count++;
    }
    if (containsRetail) {
      count++;
    }
    return count;
  }

  bool get isMixed => storeTypeCount > 1;
}

extension ExtensionSwitcher on Switcher {
  String get name {
    switch (this) {
      case Switcher.Off:
        return '關閉';
      case Switcher.On:
        return '啟用';
      default:
        return '';
    }
  }

  Color get backgroundColor {
    switch (this) {
      case Switcher.Off:
        return OKColor.GrayB9;
      case Switcher.On:
        return OKColor.Primary;
      default:
        return Colors.transparent;
    }
  }

  bool get isOn => Switcher.On == this;

  bool get isOff => Switcher.Off == this;
}

const _taxTypeNormal = 1; // 應稅
const _taxTypeZero = 2; // 零稅率
const _taxTypeFree = 3; // 免稅 (農產品)
const _taxTypeMix = 9; // 混合稅率

extension ExtensionTaxType on TaxType {
  String get name {
    switch (this) {
      case TaxType.TX:
        return '應稅';
      case TaxType.Free:
        return '免稅';
      case TaxType.Mix:
        return '混合稅率';
      default:
        return '';
    }
  }

  num get bpscmType {
    switch (this) {
      case TaxType.TX:
        return _taxTypeNormal;
      case TaxType.Free:
        return _taxTypeFree;
      case TaxType.Mix:
        return _taxTypeMix;
      default:
        return _taxTypeNormal;
    }
  }

  bool get isMix => TaxType.Mix == this;

  bool get isFree => TaxType.Free == this;

  bool get isTX => TaxType.TX == this;
}

extension ExtensionButton on Button {
  bool get isNegative => Button.Negative == this;

  bool get isPositive => Button.Positive == this;

  bool get isMiddle => Button.Middle == this;
}

extension ExtensionStoreType on StoreType {
  String get actionName {
    switch (this) {
      case StoreType.Dinner:
        return '點餐';
      case StoreType.Retail:
        return '零售';
      default:
        return '';
    }
  }

  String get displayName {
    switch (this) {
      case StoreType.Dinner:
        return '餐飲訂單';
      case StoreType.Retail:
        return '電商訂單';
      default:
        return '';
    }
  }

  String get listName {
    switch (this) {
      case StoreType.Dinner:
        return '消費紀錄';
      case StoreType.Retail:
        return '訂單記錄';
      default:
        return '';
    }
  }

  String get name {
    switch (this) {
      case StoreType.Dinner:
        return '餐飲';
      case StoreType.Retail:
        return '零售';
      default:
        return '';
    }
  }

  String get receipt {
    if (isDinner) {
      return '餐點';
    }
    if (isRetail) {
      return '商品';
    }
    return '';
  }

  String get feeName {
    switch (this) {
      case StoreType.Dinner:
        return '服務費';
      case StoreType.Retail:
        return '運費';
      default:
        return '';
    }
  }

  Color get color {
    switch (this) {
      case StoreType.Dinner:
        return Colors.transparent;
      case StoreType.Retail:
        return OKColor.Retail;
      default:
        return Colors.transparent;
    }
  }

  bool get isDinner => StoreType.Dinner == this;

  bool get isRetail => StoreType.Retail == this;

  ProductKind get productKind {
    switch (this) {
      case StoreType.Dinner:
        return ProductKind.DinnerApp;
      case StoreType.Retail:
        return ProductKind.Retail;
      default:
        return ProductKind.Max;
    }
  }
}

extension ExtensionOrderType on OrderType {
  // 特殊: 0, 6 都歸類為 0
  bool get isDinnerHere => ORDER_TYPE_DINNER_HERE.contains(this);
  bool get isDinnerOrder => OrderType.DinnerOrder == this;
  bool get isDinnerToGo => OrderType.DinnerToGo == this;
  bool get isDinnerDelivery => OrderType.DinnerDelivery == this;
  bool get isRetailToGo => OrderType.RetailToGo == this;
  bool get isRetailDelivery => OrderType.RetailDelivery == this;
  bool get isRetailInStore => OrderType.RetailInStore == this;

  String get name {
    switch (this) {
      case OrderType.DinnerHere:
        return '內用';
      case OrderType.DinnerOrder:
        return '現場點餐';
      case OrderType.DinnerToGo:
        return '自取';
      case OrderType.DinnerDelivery:
        return '外送';
      case OrderType.RetailToGo:
        return '自取';
      case OrderType.RetailDelivery:
        return '宅配';
      case OrderType.RetailInStore:
        return '超取';
      default:
        return '';
    }
  }

  Color getColorWithSource(OrderSource source) {
    if (OrderType.DinnerHere == this) {
      switch (source) {
        case OrderSource.Line: // 預約內用
          return OrderTypeColor.DinnerLineHere;
        default:
      }
    } else if (OrderType.DinnerToGo == this) {
      switch (source) {
        case OrderSource.App: // 外帶
          return OrderTypeColor.DinnerTakeout;
        case OrderSource.Line: // 自取
          return OrderTypeColor.DinnerToGo;
        default:
      }
    }
    return color;
  }

  Color get color {
    switch (this) {
      case OrderType.DinnerHere:
      case OrderType.DinnerOrder:
        return OrderTypeColor.DinnerHere;
      case OrderType.DinnerToGo:
        return OrderTypeColor.DinnerToGo;
      case OrderType.DinnerDelivery:
        return OrderTypeColor.DinnerDelivery;
      case OrderType.RetailToGo:
        return OrderTypeColor.RetailToGo;
      case OrderType.RetailDelivery:
        return OrderTypeColor.RetailDelivery;
      case OrderType.RetailInStore:
        return OrderTypeColor.RetailInStore;
      case OrderType.DinnerOrder:
      default:
        return OKColor.Error;
    }
  }

  String getNameWithSource(OrderSource value) {
    if (OrderType.DinnerHere == this) {
      switch (value) {
        case OrderSource.Line:
          return '預約';
        default:
          break;
      }
    } else if (OrderType.DinnerToGo == this) {
      switch (value) {
        case OrderSource.App:
          return '外帶';
        case OrderSource.Line:
          return '自取';
        case OrderSource.Max:
          break;
      }
    } else if (OrderType.DinnerOrder == this) {
      return '現場';
    }
    return name;
  }

  StoreType get storeType {
    if (isDinner) return StoreType.Dinner;
    if (isRetail) return StoreType.Retail;
    return StoreType.Max;
  }

  bool get isDinner => ORDER_TYPE_DINNER.contains(this);

  bool get isRetail => ORDER_TYPE_RETAIL.contains(this);

  String get mealName {
    switch (this) {
      case OrderType.DinnerHere:
      case OrderType.DinnerOrder:
        return '用餐時間：';
      case OrderType.DinnerToGo:
      case OrderType.DinnerDelivery:
        return '取餐時間：';
      case OrderType.RetailToGo:
      case OrderType.RetailDelivery:
      case OrderType.RetailInStore:
        return '取貨時間：';
      default:
        return '';
    }
  }
}

extension ExtensionOrderSource on OrderSource {
  String get name {
    switch (this) {
      case OrderSource.App:
        return 'APP 訂單';
      case OrderSource.Line:
        return 'LINE 訂單';
      default:
        return '';
    }
  }

  Color get color {
    switch (this) {
      case OrderSource.App:
        return OKColor.Primary;
      case OrderSource.Line:
        return OKColor.Line;
      default:
        return OKColor.Error;
    }
  }

  bool get isLine => OrderSource.Line == this;

  bool get isApp => OrderSource.App == this;
}

extension ExtensionLineType on LineType {
  String get name {
    switch (this) {
      case LineType.Offline:
        return '門市';
      case LineType.Online:
        return '線上';
      default:
        return '';
    }
  }
}

extension ExtensionPaymentStatus on PaymentStatus {
  bool get isOutstanding => PaymentStatus.Outstanding == this;

  bool get isBalance => PaymentStatus.Balance == this;

  String get name {
    switch (this) {
      case PaymentStatus.Outstanding:
        return '未付款';
      case PaymentStatus.Balance:
        return '未付款';
        // return '未結清';
      case PaymentStatus.Paid:
        return '已付款';
      case PaymentStatus.Failed:
        return '付款失敗';
      case PaymentStatus.Timeout:
        return '超過付款時間';
      default:
        return '';
    }
  }

  Color get color {
    switch (this) {
      case PaymentStatus.Paid:
        return OKColor.Retail;
      case PaymentStatus.Outstanding:
      case PaymentStatus.Balance:
      case PaymentStatus.Failed:
      case PaymentStatus.Timeout:
        return OKColor.Error;
      default:
        return OKColor.Error;
    }
  }

  Color get backgroundColor {
    switch (this) {
      case PaymentStatus.Paid:
        return OKColor.Primary;
      case PaymentStatus.Outstanding:
        return OKColor.GrayBF;
      case PaymentStatus.Balance:
        return OKColor.GrayBF;
      case PaymentStatus.Failed:
      case PaymentStatus.Timeout:
        return OKColor.Error;
      default:
        return OKColor.Error;
    }
  }
}

extension ExtensionAddressType on AddressType {
  String get name {
    switch (this) {
      case AddressType.Buyer:
        return '購買人';
      case AddressType.Recipient:
        return '收件人';
      default:
        return '';
    }
  }

  bool get isBuyer => AddressType.Buyer == this;

  bool get isRecipient => AddressType.Recipient == this;
}

extension ExtensionOrderStatus on OrderStatus {
  String get name {
    switch (this) {
      case OrderStatus.Padding:
        return '處理中';
      case OrderStatus.Accepted:
        return '已確認';
      case OrderStatus.Completed:
        return '訂單完成';
      case OrderStatus.CancelByApp:
        return '訂單取消';
      case OrderStatus.Exception:
        return '訂單異常';
      case OrderStatus.Rejection:
        return '訂單退貨、退款';
      case OrderStatus.CancelByLine:
        return '訂單取消';
      default:
        return '';
    }
  }

  Color get color {
    switch (this) {
      case OrderStatus.Padding:
      case OrderStatus.Accepted:
      case OrderStatus.Completed:
        return StatusColor.Normal;
      case OrderStatus.CancelByApp:
      case OrderStatus.Exception:
      case OrderStatus.Rejection:
      case OrderStatus.CancelByLine:
        return StatusColor.Error;
      default:
        return OKColor.Error;
    }
  }

  OrdersViewMode get ordersViewMode {
    if (isActive) {
      return OrdersViewMode.ActiveOrders;
    }
    if (isArchived) {
      return OrdersViewMode.CompletedOrders;
    }
    return OrdersViewMode.Max;
  }

  bool get isPadding => OrderStatus.Padding == this;
  bool get isAccepted => OrderStatus.Accepted == this;
  bool get isCompleted => OrderStatus.Completed == this;
  bool get isCancelByApp => OrderStatus.CancelByApp == this;
  bool get isException => OrderStatus.Exception == this;
  bool get isRejection => OrderStatus.Rejection == this;
  bool get isCancelByLine => OrderStatus.CancelByLine == this;
  // TODO: remove me
  bool get isUnfinished => isActive;
  bool get isActive => ORDERS_STATUS_ACTIVE.contains(this);
  bool get isCancelled => ORDERS_STATUS_CANCEL.contains(this);
  bool get isArchived => ORDERS_STATUS_ARCHIVED.contains(this);
  // TODO: remove me
  bool get isFinished => isArchived;
}

extension ExtensionAppPayMethod on AppPayMethod {
  bool get isECPay => AppPayMethod.ECPay == this;

  bool get isCash => AppPayMethod.Cash == this;

  bool get isCreditCard => AppPayMethod.CreditCard == this;

  String get name {
    switch (this) {
      case AppPayMethod.Mutilpe:
        return '多重支付';
      case AppPayMethod.Cash:
        return '現金';
      case AppPayMethod.CreditCard:
        return '信用卡';
      case AppPayMethod.TaiwanPay:
        return '台灣Pay';
      case AppPayMethod.Alipay:
        return '支付寶';
      case AppPayMethod.LinePayMoney:
        return 'Line Pay Money';
      case AppPayMethod.icash:
        return 'iCash';
      case AppPayMethod.WeChatPay:
        return '微信支付';
      case AppPayMethod.HappyCash:
        return 'Happy Cash';
      case AppPayMethod.TansferLine:
        return '轉帳匯款';
      case AppPayMethod.LinePay:
        return 'Line Pay';
      case AppPayMethod.EasyCard:
        return '悠遊卡 Easy Card';
      case AppPayMethod.JKOPAY:
        return '街口支付';
      case AppPayMethod.iPass:
        return 'iPass 一卡通';
      case AppPayMethod.Foodpanda:
        return 'Foodpanda';
      case AppPayMethod.OPay:
        return '歐付寶';
      case AppPayMethod.UberEats:
        return 'UberEats';
      case AppPayMethod.COD:
        return '貨到付款';
      case AppPayMethod.CashAtStore:
        return '現場付款';
      case AppPayMethod.ECPay:
        return '綠界信用卡';
      case AppPayMethod.TansferApp:
        return '轉帳匯款';
      case AppPayMethod.QuintupleStimulusVoucher:
        return '振興五倍券';
      default:
        return '';
    }
  }

  String get icon {
    switch (this) {
      case AppPayMethod.Mutilpe:
        return 'assets/images/pay_method_0.png';
      case AppPayMethod.Cash:
        return 'assets/images/pay_method_1.png';
      case AppPayMethod.CreditCard:
        return 'assets/images/pay_method_2.png';
      case AppPayMethod.TaiwanPay:
        return 'assets/images/pay_method_3.png';
      case AppPayMethod.Alipay:
        return 'assets/images/pay_method_4.png';
      case AppPayMethod.LinePayMoney:
        return 'assets/images/pay_method_5.png';
      case AppPayMethod.icash:
        return 'assets/images/pay_method_6.png';
      case AppPayMethod.WeChatPay:
        return 'assets/images/pay_method_7.png';
      case AppPayMethod.HappyCash:
        return 'assets/images/pay_method_8.png';
      case AppPayMethod.TansferLine:
        return 'assets/images/pay_method_9.png';
      case AppPayMethod.LinePay:
        return 'assets/images/pay_method_10.png';
      case AppPayMethod.EasyCard:
        return 'assets/images/pay_method_11.png';
      case AppPayMethod.JKOPAY:
        return 'assets/images/pay_method_12.png';
      case AppPayMethod.iPass:
        return 'assets/images/pay_method_13.png';
      case AppPayMethod.Foodpanda:
        return 'assets/images/pay_method_14.png';
      case AppPayMethod.OPay:
        return 'assets/images/pay_method_15.png';
      case AppPayMethod.UberEats:
        return 'assets/images/pay_method_16.png';
      case AppPayMethod.COD:
        return 'assets/images/pay_method_17.png';
      case AppPayMethod.CashAtStore:
        return 'assets/images/pay_method_18.png';
      case AppPayMethod.ECPay:
        return 'assets/images/pay_method_19.png';
      case AppPayMethod.TansferApp:
        return 'assets/images/pay_method_20.png';
      case AppPayMethod.QuintupleStimulusVoucher:
        return 'assets/images/pay_method_21.png';
      default:
        return '';
    }
  }
}

extension ExtensionShippingMethod on ShippingMethod {
  String get name {
    switch (this) {
      case ShippingMethod.PickUp:
        return '自取';
      case ShippingMethod.Amb:
        return '常溫';
      case ShippingMethod.Cold:
        return '低溫';
      default:
        return '';
    }
  }

  bool get isPickUp => ShippingMethod.PickUp == this;
  bool get isAmb => ShippingMethod.Amb == this;
  bool get isCold => ShippingMethod.Cold == this;
}

extension ExtensionCarrierType on CarrierType {
  String get name {
    switch (this) {
      case CarrierType.EasyCard:
        return '悠遊卡';
      case CarrierType.iPass:
        return '一卡通';
      case CarrierType.icash:
        return 'icash';
      case CarrierType.Mobile:
        return '手機';
      case CarrierType.Certification:
        return '自然人憑證';
      case CarrierType.Debit:
        return '金融卡';
      case CarrierType.Public:
        return '公用事業';
      case CarrierType.CreditCard:
        return '信用卡';
      case CarrierType.Member:
        return '會員';
      default:
        return '';
    }
  }
}

extension ExtensionInvoiceType on InvoiceType {
  String get name {
    switch (this) {
      case InvoiceType.GUIexception:
        return '免開發票';
      case InvoiceType.Invoice:
        return '個人電子發票';
      case InvoiceType.TaxId:
        return '公司電子發票';
      case InvoiceType.Carrier:
        return '手機條碼載具';
      case InvoiceType.LoveCode:
        return '愛心碼';
      case InvoiceType.Paper:
        return '紙本發票';
      default:
        return '';
    }
  }
}

extension ExtensionInvoiceStatus on InvoiceStatus {
  String get name {
    switch (this) {
      case InvoiceStatus.Invoice:
        return '開立';
      case InvoiceStatus.Cancel:
        return '作廢';
      case InvoiceStatus.Discount:
        return '折讓';
      default:
        return '';
    }
  }

  bool get isInvoice => InvoiceStatus.Invoice == this;
  bool get isCancel => InvoiceStatus.Cancel == this;
  bool get isDiscount => InvoiceStatus.Discount == this;
}

extension ExtensionItemType on ItemType {
  String get name {
    switch (this) {
      case ItemType.Normal:
        return '一般';
      case ItemType.PreOrder:
        return '預購';
      case ItemType.OneMore:
        return '加購';
      case ItemType.Giveaway:
        return '贈品';
      case ItemType.Additional:
        return '額外費用';
      case ItemType.ServiceFee:
        return '服務費';
      default:
        return '';
    }
  }

  bool get isNormal => ItemType.Normal == this;
  bool get isPreOrder => ItemType.PreOrder == this;
  bool get isOneMore => ItemType.OneMore == this;
  bool get isGiveaway => ItemType.Giveaway == this;
  bool get isAdditional => ItemType.Additional == this;
  bool get isServiceFee => ItemType.ServiceFee == this;
}

extension ExtensionDiscountType on DiscountType {
  String get name {
    switch (this) {
      case DiscountType.Coupon:
        return 'coupon';
      case DiscountType.Promotion:
        return 'promotion';
      case DiscountType.Reset:
        return '整筆改價';
      case DiscountType.Discount:
        return '現場折價';
      default:
        return '';
    }
  }
}

extension ExtensionServiceFee on ServiceFeeType {
  bool get hasServiceFee => !isNone;

  bool get isNone => ServiceFeeType.None == this;

  bool get isOrigin => ServiceFeeType.Origin == this;

  bool get isDiscount => ServiceFeeType.Discount == this;
}

extension ExtensionContentType on ContentType {
  String get name {
    switch (this) {
      case ContentType.Order:
        return '訂單';
      case ContentType.Member:
        return '會員';
      case ContentType.Coupon:
        return '一般優惠券';
      case ContentType.CooperationCoupon:
        return '跨界優惠券';
      case ContentType.QuestionnaireCoupon:
        return '問券優惠券';
      case ContentType.Questionnaire:
        return '問券核銷';
      default:
        return '';
    }
  }
}

extension ExtensionGender on GenderType {
  String get name {
    switch (this) {
      case GenderType.Male:
        return '男';
      case GenderType.Female:
        return '女';
      case GenderType.Unknow:
        return '不提供';
      default:
        return '';
    }
  }
}

extension ExtensionPointType on PointType {
  String get name {
    switch (this) {
      case PointType.Income:
        return '獲得積點';
      case PointType.Expense:
        return '扣除積點';
      case PointType.Expiry:
        return '過期';
      default:
        return '';
    }
  }
}

extension ExtensionOrdersBusinessHoursMode on OrdersBusinessHoursMode {
  String get displayName {
    switch (this) {
      case OrdersBusinessHoursMode.Current:
        return '本時段單';
      case OrdersBusinessHoursMode.Other:
        return '下時段單';
      default:
        return '';
    }
  }

  String get toggleDisplayName {
    switch (this) {
      case OrdersBusinessHoursMode.Current:
        return '下時段單';
      case OrdersBusinessHoursMode.Other:
        return '本時段單';
      default:
        return '';
    }
  }

  bool get isCurrent => OrdersBusinessHoursMode.Current == this;
  bool get isOther => OrdersBusinessHoursMode.Other == this;
  bool get isAll => OrdersBusinessHoursMode.All == this;
}

extension ExtensionOrdersViewMode on OrdersViewMode {
  String get name => describeEnum(this);

  String get sortType {
    switch (this) {
      case OrdersViewMode.ActiveOrders:
        return 'meal_at';
      case OrdersViewMode.CompletedOrders:
        // return 'order_number';
        return '';
      default:
        return 'order_number';
    }
  }

  String get sort {
    switch (this) {
      case OrdersViewMode.ActiveOrders:
        return 'ASC';
      case OrdersViewMode.CompletedOrders:
        // return 'DESC';
        return '';
      default:
        return 'DESC';
    }
  }

  List<OrderStatus> get status {
    switch (this) {
      case OrdersViewMode.ActiveOrders:
        return ORDERS_STATUS_ACTIVE;
      case OrdersViewMode.CompletedOrders:
        return ORDERS_STATUS_ARCHIVED;
      default:
        return const <OrderStatus>[];
    }
  }

  bool get isActiveOrders => OrdersViewMode.ActiveOrders == this;
  bool get isCompletedOrders => OrdersViewMode.CompletedOrders == this;
}

extension ExtensionPromotionType on PromotionType {
  String get name {
    switch (this) {
      case PromotionType.Off:
        return '打折';
      case PromotionType.Discount:
        return '折抵';
      case PromotionType.Gift:
        return '贈送商品';
      case PromotionType.Upgrade:
        return '升級商品';
      default:
        return '';
    }
  }

  String get title {
    switch (this) {
      case PromotionType.Off:
        return '商品折扣';
      case PromotionType.Discount:
        return '現場減價';
      case PromotionType.Gift:
        return '贈送商品';
      case PromotionType.Upgrade:
        return '額外費用';
      default:
        return '';
    }
  }

  bool get isOff => PromotionType.Off == this;
  bool get isDiscount => PromotionType.Discount == this;
  bool get isGift => PromotionType.Gift == this;
  bool get isUpgrade => PromotionType.Upgrade == this;
  bool contains(List<PromotionType> value) => value.contains(this);
}

extension ExtensionCouponStatus on CouponStatus {
  String get name {
    switch (this) {
      case CouponStatus.Unavailable:
        return '失效';
      case CouponStatus.Available:
        return '可用';
      case CouponStatus.Used:
        return '已使用';
      default:
        return '';
    }
  }

  bool get isAvailable => CouponStatus.Available == this;
  bool get isUnavailable => CouponStatus.Unavailable == this;
  bool get isUsed => CouponStatus.Used == this;
}

extension ExtensionBpscmTaxType on BpscmTaxType {
  bool get isTX => BpscmTaxType.TX == this;
  bool get isZero => BpscmTaxType.Zero == this;
  bool get isFree => BpscmTaxType.Free == this;
  bool get isSpecial => BpscmTaxType.Special == this;
  bool get isMix => BpscmTaxType.Mix == this;

  String get name {
    switch (this) {
      case BpscmTaxType.TX:
        return '應稅';
      case BpscmTaxType.Zero:
        return '零稅率';
      case BpscmTaxType.Free:
        return '免稅';
      case BpscmTaxType.Special:
        return '應稅';
      case BpscmTaxType.Mix:
        return '混合稅率';
      default:
        return '';
    }
  }

  String get str {
    switch (this) {
      case BpscmTaxType.TX:
        return '${BpscmTaxType.TX.value}';
      case BpscmTaxType.Zero:
        return '${BpscmTaxType.Zero.value}';
      case BpscmTaxType.Free:
        return '${BpscmTaxType.Free.value}';
      case BpscmTaxType.Special:
        return '${BpscmTaxType.Special.value}';
      case BpscmTaxType.Mix:
        return '${BpscmTaxType.Mix.value}';
      default:
        return '';
    }
  }

  String get tail => BpscmTaxType.TX == this ? ' TX' : '';
}

extension ExtensionBpscmInvoiceStatus on BpscmInvoiceStatus {
  String get name {
    switch (this) {
      case BpscmInvoiceStatus.Invoice:
        return '發票開立';
      case BpscmInvoiceStatus.Cancel:
        return '發票作廢';
      case BpscmInvoiceStatus.Allowance:
        return '折讓開立';
      case BpscmInvoiceStatus.AllowanceCancel:
        return '折讓作廢';
      default:
        return '';
    }
  }
}

extension ExtensionSortType on SortType {
  String get name {
    switch (this) {
      case SortType.DESC:
        return 'DESC';
      case SortType.ASC:
        return 'ASC';
      default:
        return '';
    }
  }
}

extension ExtensionPrinterPaperSize on PrinterPaperSize {
  String get name {
    switch (this) {
      case PrinterPaperSize.mm58:
        return '58mm';
      case PrinterPaperSize.mm80:
        return '80mm';
      default:
        return '';
    }
  }
}
