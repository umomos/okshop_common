// 要賦予特殊值使用
// ref: https://stackoverflow.com/a/13901969
// The old approach before 1.8:
// class TaxType {
//   final int value;
//   const TaxType._internal(this.value);
//   static const TX = TaxType._internal(1); // 應稅
//   static const Free = TaxType._internal(2); // 免稅
//   static const Mix = TaxType._internal(3); // 混合稅率
// }

// Beginning 1.8

// 角色
enum StoreRole {
  None,
  Boss, // 1: 店長
  Employee, // 2: 店員
  Max,
}

// 產品分類
enum ProductKind {
  DinnerApp, // 0: 餐飲店內
  DinnerLine, // 1: 餐飲線上
  Retail, // 2: 零售
  Max,
}

// 選項
enum Selection {
  Single, // 0: 單選
  Multiple, // 1: 複選
  Max,
}

enum Requirement {
  NiceToHave, // 0: 非必填
  MustToHave, // 1: 必填
  Max,
}

// 物流類別
enum ShippingType {
  Normal, // 0: 常溫 (default)
  Cold, // 1: 低溫
  Max,
}

// 品牌業態(結帳類型)
enum BrandsType {
  BeforeDinner, // 0: 餐飲-前結帳
  AfterDinner, // 1: 餐飲-後結帳
  Retail, // 2: 零售
  BeforeDinnerWithRetail, // 3: 餐飲-前結帳+零售
  AfterDinnerWithRetail, // 4: 餐飲-後結帳+零售
  DinnerOnlineOnly, // 5: 僅線上 - 餐飲
  RetailOnlineOnly, // 6: 僅線上 - 零售
  DinnerAndRetailOnlineOnly, // 7: 僅線上 - 餐飲+零售
  Max,
}

// 開關
enum Switcher {
  Off, // 0: 關閉
  On, // 1: 開啟
  Max,
}

// 稅金
enum TaxType {
  None,
  TX, // 1: 應稅
  Free, // 2: 免稅
  Mix, // 3: 混合稅率
  Max,
}

// 金財通
class BpscmTaxType {
  final num value;
  const BpscmTaxType._internal(this.value);
  static const None = BpscmTaxType._internal(0); // 無
  static const TX = BpscmTaxType._internal(1); // 應稅
  static const Zero = BpscmTaxType._internal(2); // 零稅率
  static const Free = BpscmTaxType._internal(3); // 免稅 (農產品)
  static const Special = BpscmTaxType._internal(4); // 應稅 (特種稅率)
  static const Mix = BpscmTaxType._internal(9); // 混合稅率
}

// 稅率
class TaxRate {
  final num value;
  const TaxRate._internal(this.value);
  static const None = TaxRate._internal(0.0); // 無
  static const TX = TaxRate._internal(0.05); // 應稅
}

enum Button {
  Negative, // 0: 取消
  Positive, // 1: 確認
  Middle, // 2:
  Max,
}

// 商店類型
enum StoreType {
  Dinner, // 0: 餐飲
  Retail, // 1: 零售
  Max,
}

// 訂單類型
enum OrderType {
  DinnerHere, // 0: 餐飲內用
  DinnerToGo, // 1: 餐飲外帶/餐飲自取
  DinnerDelivery, // 2: 餐飲外送
  RetailToGo, // 3: 零售自取
  RetailDelivery, // 4: 零售宅配
  RetailInStore, // 5: 零售超商
  DinnerOrder, // 6: 現場點餐 (歸類為 0: 餐飲內用)
  Max,
}

// 訂單來源
enum OrderSource {
  App, // 0: APP 訂單
  Line, // 1: LINE 訂單
  Max,
}

// 門市/線上 (same with OrderSource)
enum LineType {
  Offline, // 0: 門市
  Online, // 1: 線上
  Max,
}

// 付款狀態
enum PaymentStatus {
  Outstanding, // 0: 未付款
  Balance, // 1: 未結清
  Paid, // 2: 已付款
  Failed, // 3: 付款失敗
  Timeout, // 4: 超過付款時間
  Max,
}

enum AddressType {
  Buyer, // 0: 購買人
  Recipient, // 1: 收件人
  Max,
}

// 訂單狀態
enum OrderStatus {
  Padding, // 0: 處理中
  Accepted, // 1: 已確認
  Completed, // 2: 訂單完成
  CancelByApp, // 3: 訂單取消 (店家)
  Exception, // 4: 訂單異常
  Rejection, // 5: 訂單退貨、退款
  CancelByLine, // 6: 訂單取消 (消費者)
  Max,
}

// TODO: 更名 AppPayMethod
// 線下金流類型 (APP)
enum AppPayMethod {
  Mutilpe,
  Cash, // 1: 現金
  CreditCard, // 2: 信用卡
  TaiwanPay, // 3: 台灣Pay
  Alipay, // 4: 支付寶
  LinePayMoney, // 5: Line Pay Money
  icash, // 6: iCash
  WeChatPay, // 7: 微信支付
  HappyCash, // 8: Happy Cash
  TansferLine, // 9: 轉帳匯款
  LinePay, // 10: Line Pay
  EasyCard, // 11: 悠遊卡 Easy Card
  JKOPAY, // 12: 街口支付
  iPass, // 13: iPass 一卡通
  Foodpanda, // 14: Foodpanda
  OPay, // 15: 歐付寶
  UberEats, // 16: UberEats 線上金流類型 (WEB)
  COD, // 17: 貨到付款
  CashAtStore, // 18: 到店付款
  ECPay, // 19: 綠界信用卡
  TansferApp, // 20: 轉帳匯款
  QuintupleStimulusVoucher, // 21: 五倍券
  Max,
}

// 物流類型
enum ShippingMethod {
  None,
  PickUp, // 1: 自取
  Amb, // 2: 宅配：常溫
  Cold, // 3: 宅配：低溫
  Max,
}

// 載具類別
enum CarrierType {
  EasyCard, // 0: 悠遊卡
  iPass, // 1: 一卡通
  icash, // 2: icash
  Mobile, // 3: 手機
  Certification, // 4: 自然人憑證
  Debit, // 5: 金融卡
  Public, // 6: 公用事業
  CreditCard, // 7: 信用卡
  Member, // 8: 會員
  Max,
}

// 發票類型
enum InvoiceType {
  GUIexception, // 0: 免開發票 GUI(GovernmentUniformInvoice)
  Invoice, // 1: 個人電子發票
  TaxId, // 2: 公司電子發票/統編
  Carrier, // 3: 手機條碼載具
  LoveCode, // 4: 愛心碼
  Paper, // 5: 紙本發票
  Max,
}

// 發票狀態
enum InvoiceStatus {
  Invoice, // 0: 開立
  Cancel, // 1: 作廢
  Discount, // 2: 折讓
  Max,
}

// 產品類型
enum ItemType {
  Normal, // 0: 一般
  PreOrder, // 1: 預購
  OneMore, // 2: 加購
  Giveaway, // 3: 贈品
  Additional, // 4: 額外費用
  ServiceFee, // 5: 服務費
  Max,
}

// 折扣類別
enum DiscountType {
  Coupon, // 0: coupon
  Promotion, // 1: promotion
  Reset, // 2: 整筆改價
  Discount, // 3: 現場折價
  Max,
}

// 服務費
enum ServiceFeeType {
  None, // 0: 關閉
  Origin, // 1: 以原價計算
  Discount, // 2: 以折扣後價格算
  Max,
}

enum Times {
  First, // 0: 首次
  Again, // 1: 再次
  Max,
}

// 執行內容
enum ContentType {
  Order, // 訂單
  Member, // 會員
  Coupon, // 一般優惠券
  CooperationCoupon, // 跨界優惠券
  QuestionnaireCoupon, // 問券優惠券
  Questionnaire, // 問券核銷
}

enum ActionType {
  Create,
  Update,
  Delete,
}

enum GenderType {
  Female, // 0: 女
  Male, // 1: 男
  Unknow, // 2: 不提供
  Max,
}

enum PointType {
  Income, // 0: 收入
  Expense, // 1: 支出
  Expiry, // 2: 過期
  Max,
}

// 餐飲模式時段顯示設定變數
enum OrdersBusinessHoursMode {
  Current, // 0: 本時段單
  Other, // 1: 其他時段單 (雖然名稱很智障的寫成下時段單,實際需求卻不是這樣)
  All, // 2: 全部時段
  Max,
}

// 訂單列表運作模式
enum OrdersViewMode {
  ActiveOrders, // 0: 當前正在處理的訂單
  CompletedOrders, // 1: 完成的訂單 (歷史紀錄)
  Max,
}

enum PromotionType {
  Off, // 0: 打折
  Discount, // 1: 折抵
  Gift, // 2: 贈送商品
  Upgrade, // 3: 升級商品
  Max,
}

enum CouponStatus {
  Unavailable, // 0: 失效
  Available, // 1: 可用
  Used, // 2: 已使用
  Max,
}

class BpscmInvoiceStatus {
  final num value;
  const BpscmInvoiceStatus._internal(this.value);
  static const Invoice = BpscmInvoiceStatus._internal(1);
  static const Cancel = BpscmInvoiceStatus._internal(2);
  static const Allowance = BpscmInvoiceStatus._internal(4);
  static const AllowanceCancel = BpscmInvoiceStatus._internal(5);
}

enum SortType {
  DESC, // 0: 倒排序
  ASC, // 1: 正排序
  Max,
}

enum DataAction {
  Create,
  Read,
  Update,
  Delete,
}

enum PrinterPaperSize {
  None, // 0: 未設定
  mm58, // 1:
  mm80, // 2:
  Max,
}
