import 'package:intl/intl.dart';

import 'enums.dart';

final _currencyFormat = NumberFormat.currency(
  locale: 'en_US',
  symbol: '\$',
  decimalDigits: 0,
);

final _decimalFormat = NumberFormat.currency(
  locale: 'en_US',
  symbol: '',
  decimalDigits: 0,
);

final _decimalFormat2 = NumberFormat('#,##0.##');

extension ExtensionNum on num {
  CouponStatus get couponStatus {
    if (this != null && this >= 0 && this < CouponStatus.values.length) {
      return CouponStatus.values.elementAt(this);
    }
    return CouponStatus.Max;
  }

  PromotionType get promotionType {
    if (this != null && this >= 0 && this < PromotionType.values.length) {
      return PromotionType.values.elementAt(this);
    }
    return PromotionType.Max;
  }

  LineType get lineType {
    if (this != null && this >= 0 && this < LineType.values.length) {
      return LineType.values.elementAt(this);
    }
    return LineType.Max;
  }

  PointType get pointType {
    if (this != null && this >= 0 && this < PointType.values.length) {
      return PointType.values.elementAt(this);
    }
    return PointType.Max;
  }

  GenderType get genderType {
    if (this != null && this >= 0 && this < GenderType.values.length) {
      return GenderType.values.elementAt(this);
    }
    return GenderType.Max;
  }

  String get floatingStyle => _decimalFormat2.format(this ?? 0);

  String get decimalStyle => _decimalFormat.format(this ?? 0);

  String get currencyStyle => _currencyFormat.format(this ?? 0);

  OrderSource get orderSource {
    if (this != null && this >= 0 && this < OrderSource.values.length) {
      return OrderSource.values.elementAt(this);
    }
    return OrderSource.Max;
  }

  OrderType get orderType {
    if (this != null && this >= 0 && this < OrderType.values.length) {
      return OrderType.values.elementAt(this);
    }
    return OrderType.Max;
  }

  OrderStatus get orderStatus {
    if (this != null && this >= 0 && this < OrderStatus.values.length) {
      return OrderStatus.values.elementAt(this);
    }
    return OrderStatus.Max;
  }

  AddressType get addressType {
    if (this != null && this >= 0 && this < AddressType.values.length) {
      return AddressType.values.elementAt(this);
    }
    return AddressType.Max;
  }

  CarrierType get carrierType {
    if (this != null && this >= 0 && this < CarrierType.values.length) {
      return CarrierType.values.elementAt(this);
    }
    return CarrierType.Max;
  }

  ShippingMethod get shippingMethod {
    if (this != null && this >= 0 && this < ShippingMethod.values.length) {
      return ShippingMethod.values.elementAt(this);
    }
    return ShippingMethod.Max;
  }

  TaxType get taxType {
    if (this != null && this >= 0 && this < TaxType.values.length) {
      return TaxType.values.elementAt(this);
    }
    return TaxType.Max;
  }

  DiscountType get discountType {
    if (this != null && this >= 0 && this < DiscountType.values.length) {
      return DiscountType.values.elementAt(this);
    }
    return DiscountType.Max;
  }

  ItemType get itemType {
    if (this != null && this >= 0 && this < ItemType.values.length) {
      return ItemType.values.elementAt(this);
    }
    return ItemType.Max;
  }

  InvoiceStatus get invoiceStatus {
    if (this != null && this >= 0 && this < InvoiceStatus.values.length) {
      return InvoiceStatus.values.elementAt(this);
    }
    return InvoiceStatus.Max;
  }

  InvoiceType get invoiceType {
    if (this != null && this >= 0 && this < InvoiceType.values.length) {
      return InvoiceType.values.elementAt(this);
    }
    return InvoiceType.Max;
  }

  StoreRole get storeRole {
    if (this != null && this >= 0 && this < StoreRole.values.length) {
      return StoreRole.values.elementAt(this);
    }
    return StoreRole.Max;
  }

  ProductKind get productKind {
    if (this != null && this >= 0 && this < ProductKind.values.length) {
      return ProductKind.values.elementAt(this);
    }
    return ProductKind.Max;
  }

  Selection get selection {
    if (this != null && this >= 0 && this < Selection.values.length) {
      return Selection.values.elementAt(this);
    }
    return Selection.Max;
  }

  Requirement get requirement {
    if (this != null && this >= 0 && this < Requirement.values.length) {
      return Requirement.values.elementAt(this);
    }
    return Requirement.Max;
  }

  ShippingType get shippingType {
    if (this != null && this >= 0 && this < ShippingType.values.length) {
      return ShippingType.values.elementAt(this);
    }
    return ShippingType.Max;
  }

  BrandsType get brandsType {
    if (this != null && this >= 0 && this < BrandsType.values.length) {
      return BrandsType.values.elementAt(this);
    }
    return BrandsType.Max;
  }

  Switcher get switcher {
    if (this != null && this >= 0 && this < Switcher.values.length) {
      return Switcher.values.elementAt(this);
    }
    return Switcher.Max;
  }

  Button get button {
    if (this != null && this >= 0 && this < Button.values.length) {
      return Button.values.elementAt(this);
    }
    return Button.Max;
  }

  PaymentStatus get paymentStatus {
    if (this != null && this >= 0 && this < PaymentStatus.values.length) {
      return PaymentStatus.values.elementAt(this);
    }
    return PaymentStatus.Max;
  }

  AppPayMethod get appPayMethod {
    if (this != null && this >= 0 && this < AppPayMethod.values.length) {
      return AppPayMethod.values.elementAt(this);
    }
    return AppPayMethod.Max;
  }

  ServiceFeeType get serviceFeeType {
    if (this != null && this >= 0 && this < ServiceFeeType.values.length) {
      return ServiceFeeType.values.elementAt(this);
    }
    return ServiceFeeType.Max;
  }

  Times get times {
    if (this != null && this >= 0 && this < Times.values.length) {
      return Times.values.elementAt(this);
    }
    return Times.Max;
  }

  StoreType get storeType {
    if (this != null && this >= 0 && this < StoreType.values.length) {
      return StoreType.values.elementAt(this);
    }
    return StoreType.Max;
  }

  BpscmTaxType get bpscmTaxType {
    if (BpscmTaxType.TX.value == this) {
      return BpscmTaxType.TX;
    } else if (BpscmTaxType.Zero.value == this) {
      return BpscmTaxType.Zero;
    } else if (BpscmTaxType.Free.value == this) {
      return BpscmTaxType.Free;
    } else if (BpscmTaxType.Special.value == this) {
      return BpscmTaxType.Special;
    } else if (BpscmTaxType.Mix.value == this) {
      return BpscmTaxType.Mix;
    } else {
      return BpscmTaxType.None;
    }
  }

  SortType get sortType {
    if (this != null && this >= 0 && this < SortType.values.length) {
      return SortType.values.elementAt(this);
    }
    return SortType.Max;
  }

  PrinterPaperSize get printerPaperSize {
    if (this != null && this >= 0 && this < PrinterPaperSize.values.length) {
      return PrinterPaperSize.values.elementAt(this);
    }
    return PrinterPaperSize.Max;
  }
}
