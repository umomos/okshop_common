// const kPosBAN = '83193989';
import 'enums.dart';

const BRANDS_TYPE_DINNER = [
  BrandsType.BeforeDinner,
  BrandsType.AfterDinner,
  BrandsType.BeforeDinnerWithRetail,
  BrandsType.AfterDinnerWithRetail,
  BrandsType.DinnerOnlineOnly,
  BrandsType.DinnerAndRetailOnlineOnly,
];

const BRANDS_TYPE_RETAIL = [
  BrandsType.Retail,
  BrandsType.BeforeDinnerWithRetail,
  BrandsType.AfterDinnerWithRetail,
  BrandsType.RetailOnlineOnly,
  BrandsType.DinnerAndRetailOnlineOnly,
];

const ORDER_TYPE_DINNER_HERE = [
  OrderType.DinnerHere,
  OrderType.DinnerOrder,
];

const ORDER_TYPE_DINNER = [
  OrderType.DinnerDelivery,
  OrderType.DinnerHere,
  OrderType.DinnerToGo,
  OrderType.DinnerOrder,
];

const ORDER_TYPE_RETAIL = [
  OrderType.RetailToGo,
  OrderType.RetailDelivery,
  OrderType.RetailInStore,
];

const ORDERS_STATUS_ACTIVE = [
  OrderStatus.Padding,
  OrderStatus.Accepted,
];

const ORDERS_STATUS_ARCHIVED = [
  OrderStatus.Completed,
  OrderStatus.CancelByApp,
  OrderStatus.Exception,
  OrderStatus.Rejection,
  OrderStatus.CancelByLine,
];

const ORDERS_STATUS_CANCEL = [
  OrderStatus.CancelByApp,
  OrderStatus.CancelByLine,
];
