// Copyright 2013 The Flutter Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'dart:ui' show Color;

import 'package:flutter/material.dart' show Colors;

// ignore_for_file: public_member_api_docs

/// Provides a collection of [Color] constants corresponding to the CSS named
/// colors.
class OKColor {
  final Color value;
  const OKColor() : value = Colors.red;
  // const OKColor._internal(this.value);
  static const Tab = Color(0xff3e4b5a);
  static const Must = Color(0xffe00707);
  static const Error = Color(0xFFE02020);
  static const Line = Color(0xFF00c330);
  static const Primary = Color(0xFFF89328);
  static const PrimaryWeight = Color(0xFFE66F53);
  static const Secondary = Color(0xFFF7AB00);
  static const Accent = Color(0xFFED4C00);
  static const Vip = Color(0xfffa5700);
  static const Retail = Color(0xFFE0A471);
  static const Shadow = Color(0x29000000);
  static const Destructive = Error;
  static const GrayBF = Color(0xffbfbfbf);
  static const GrayDD = Color(0xffdddddd);
  static const Gray22 = Color(0xff222222);
  static const Gray33 = Color(0xff333333);
  static const Gray4D = Color(0xff4d4d4d);
  static const Gray66 = Color(0xff666666);
  static const GrayB9 = Color(0xffb9b9b9);
  static const Gray58 = Color(0xff585858);
  static const Gray70 = Color(0xFF707070);
  static const GrayF7 = Color(0xFFF7F7F7);
}

class OrderTypeColor extends OKColor {
  static const DinnerHere = OKColor.Primary; // 0: 餐飲內用
  static const DinnerToGo = Color(0xFF95D10F); // 1: 餐飲自取
  static const DinnerTakeout = Color(0xFF7C6DB1); // 1: 餐飲外帶
  static const DinnerDelivery = Color(0xFF26C1CE); // 2: 餐飲外送
  static const RetailToGo = Color(0xFFE0A471); // 3: 零售自取
  static const RetailDelivery = Color(0xFFC3664B); // 4: 零售宅配
  static const RetailInStore = OKColor.Error; // 5: 零售超商
  static const DinnerOrder = OKColor.Primary; // 6: 現場點餐
  static const DinnerLineHere = Color(0xFF5985BC); // 0: 預約內用 (line)
}

class StatusColor extends OKColor {
  static const Normal = Color(0xFF6D7278);
  static const Error = OKColor.Error;
}
