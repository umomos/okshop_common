import 'package:intl/intl.dart';

import 'enums.dart';

extension ExtensionString on String {
  String takeLast(num n) {
    final padString = padLeft(n);
    return padString.substring(padString.length - n);
  }

  String get filename {
    return split('/').last;
  }

  String get path {
    final index = lastIndexOf('/');
    return index >= 0 ? substring(0, index) : '';
  }

  DateTime get utcAt {
    return DateTime.tryParse('${this}z') ?? DateTime.tryParse('$this');
  }

  DateTime get localAt => utcAt?.toLocal();
}

extension ExtensionBool on bool {
  Switcher get switcher {
    if (true == this) {
      return Switcher.On;
    }
    if (false == this) {
      return Switcher.Off;
    }
    return Switcher.Max;
  }
}

// 09-14 16:00
final _HHmm = DateFormat("HH:mm");
// 09-14 16:00
final _MM_ddHHmm = DateFormat("MM-dd HH:mm");
// 2021/09/14 16:00:15
final _yMMddHHmmss = DateFormat('yyyy/MM/dd HH:mm:ss');
// 2021-09-14 16:00:15
final _y_MM_ddHHmmss = DateFormat('yyyy-MM-dd HH:mm:ss');
// 2021-09-14 16:00
final _y_MM_ddHHmm = DateFormat('yyyy-MM-dd HH:mm:ss');
// 2021-09-03
final _y_MM_dd = DateFormat("y-MM-dd");
// 9月14日 週二
final _ymw = DateFormat.MMMEd('zh_TW');
// 2021/9/14 16:00
final _yMdHHmm = DateFormat('y/M/d HH:mm');

extension ExtensionDateTime on DateTime {
  // 2021/09/14 16:00:15
  String get yMMddHHmmss => _yMMddHHmmss.format(this);
  // 2021/9/14 16:00
  String get yMdHHmm => _yMdHHmm.format(this);
  // 2021-09-14 16:00:15
  String get y_MM_ddHHmmss => _y_MM_ddHHmmss.format(this);
  // 2021-09-14 16:00:15
  String get y_MM_ddHHmm => _y_MM_ddHHmm.format(this);
  // 2021-09-14
  String get y_MM_dd => _y_MM_dd.format(this);
  // 09-14 16:00
  String get HHmm => _HHmm.format(this);
  // 09-14 16:00
  String get MM_ddHHmm => _MM_ddHHmm.format(this);
  // 9月14日 週二
  String get MMMEd => _ymw.format(this);
}

extension ExtensionIterable<T> on Iterable<T> {
  Iterable<T> distinct<R>([R func(T t)]) {
    final entries = map((e) => MapEntry(func?.call(e) ?? e, e));
    return Map.fromEntries(entries).values;
  }
}

extension ExtensionList<T> on List<T> {
  // void distinct<R>([R func(T t)]) {
  //   final entries = map((e) => MapEntry(func?.call(e) ?? e, e));
  //   final iterable = Map.fromEntries(entries).values;
  //   clear();
  //   addAll(iterable);
  // }
}

extension ExtensionMap<K, V> on Map<K, V> {
  void removeNull() {
    removeWhere((key, value) => key == null || value == null);
  }

  Map<String, String> toStringMap() {
    return map(
        (key, value) => MapEntry('$key', value == null ? null : '$value'));
  }
}
